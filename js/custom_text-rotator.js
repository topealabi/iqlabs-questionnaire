/*------------------------------------------------------------------
Project:        Wolfram
Author:         Yevgeny Simzikov
URL:            http://simpleqode.com/
                https://twitter.com/YevSim
                https://www.facebook.com/simpleqode
Version:        2.2.1
Created:        18/08/2014
Last change:    15/06/2016
-------------------------------------------------------------------*/

/* -------------------- *\
    #MORPHEXT TEXT ROTATOR
\* -------------------- */

/**
 * Requires morphext.js, morphext.css, animate.css
 */

$(".js-rotating").Morphext({
    animation: "fadeIn", // default "bounceIn"
    separator: "|", // default ","
    speed: 4000 // default 2000
});

/**
 * Plugin documentation: https://github.com/MrSaints/Morphext
 * Full list of animations: http://daneden.github.io/animate.css/
 */